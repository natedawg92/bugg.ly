<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the userName and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	*/
	
	private $_id;
	private $user;
	
	public function authenticate(){
		$input=$this->username;
		$usernameUser=User::model()->find('userName=?',array($input));
		$emailUser = User::model()->find('email=?', array($input));
		if(isset($usernameUser)){
			$user = $usernameUser;
		}else if (isset($emailUser)){
			$user = $emailUser;
		}
		if (!isset($user->userName)){
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		} else if (!$user->validatePassword($this->password, $user->password)){
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		} else {
			$this->_id=$user->userid;
			$this->username=$user->userName;
			$this->errorCode=self::ERROR_NONE;
			$roles=Yii::app()->authManager->getRoles($this->_id);
			if(isset($roles)){
				foreach ($roles as $role){
					$role = $role->getName();
					$this->setState('roles', $role);
				}
			} else {
				$this->setState('roles', 'authenticated');
			}
		}
		return !$this->errorCode;
	}
	
	public function getId()
    {
        return $this->_id;
    }
	
}