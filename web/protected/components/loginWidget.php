<?php 
class LoginWidget extends CWidget {
	public function run(){
		$model = new LoginForm;
		$form= $this->beginWidget('CActiveForm', array(
			'id'=>'login-form',
			'action'=>array('site/login'),
			'enableClientValidation'=>true,
			'clientOptions'=>array('validateOnSubmit'=>true,)
		));
		echo $form->textField($model,'username',array('placeholder'=>'Enter username'));
		echo $form->passwordField($model,'password',array('placeholder'=>'Enter Password'));
		echo CHtml::submitButton('Login');
		$this->endWidget();
	}
} 
?>