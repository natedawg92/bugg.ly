<?php

/**
 * This is the model class for table "User".
 *
 * The followings are the available columns in table 'User':
 * @property integer $userid
 * @property string $userName
 * @property string $firstName
 * @property string $lastName
 * @property string $password
 * @property string $email
 * @property integer $avatarid
 *
 * The followings are the available model relations:
 * @property AuthAssignment[] $authAssignments
 * @property BugRevision[] $bugRevisions
 * @property Invitations[] $invitations
 * @property Invitations[] $invitations1
 * @property Project[] $projects
 * @property ScreenShot[] $screenShots
 * @property Team[] $teams
 * @property Avatar $avatar
 */
class User extends CActiveRecord
{
	public $password_repeat;
	public $password1;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'User';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('userName, firstName, lastName, password1, password_repeat, email', 'required'),
			array('avatarid', 'numerical', 'integerOnly'=>true),
			array('userName, firstName, lastName', 'length', 'max'=>45),
			array('password1, password_repeat', 'length', 'min'=> 8, 'max'=>30, 'message'=>"Passwords must be at least 8 characters long and a maximum of 30 characters."),
			array('email', 'length', 'max'=>100),
			array('email, userName', 'unique'),
			array('email', 'email'),
			array('password_repeat', 'compare', 'compareAttribute'=>'password1', 'message'=>"Passwords don't match"),
			array('password', 'hashPassword', 'on'=>'save'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('userid, userName, firstName, lastName, password, email, avatarid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'authAssignments' => array(self::HAS_MANY, 'AuthAssignment', 'userid'),
			'bugRevisions' => array(self::HAS_MANY, 'BugRevision', 'revisedBy'),
			'invitations' => array(self::HAS_MANY, 'Invitations', 'sender'),
			'invitations1' => array(self::HAS_MANY, 'Invitations', 'invitee'),
			'projects' => array(self::HAS_MANY, 'Project', 'projectOwner'),
			'screenShots' => array(self::HAS_MANY, 'ScreenShot', 'uploader'),
			'teams' => array(self::MANY_MANY, 'Team', 'TeamMembers(userid, teamid)'),
			'avatar' => array(self::BELONGS_TO, 'Avatar', 'avatarid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'userid' => 'User ID',
			'userName' => 'User Name',
			'firstName' => 'First Name',
			'lastName' => 'Surname',
			'password1' => 'Password',
			'password_repeat' => 'Confrim Password',
			'email' => 'Email Address',
			'avatarid' => 'Avatar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('userid',$this->userid);
		$criteria->compare('userName',$this->userName,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('avatarid',$this->avatarid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__){
		return parent::model($className);
	}

	public function validatePassword($password){
        return CPasswordHelper::verifyPassword($password,$this->password);
    }
 
    public function hashPassword($password){
        return CPasswordHelper::hashPassword($password);
    }
}
