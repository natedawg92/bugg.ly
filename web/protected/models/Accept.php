<?php

class Accept extends CFormModel{

	public $accept;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('accept', 'safe'),
		);
	}

	public function attributeLAbels(){
		return array(
				'accept' => 'Do you accept this invitation',
			);
	}
}
?>