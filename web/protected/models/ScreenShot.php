<?php

/**
 * This is the model class for table "ScreenShot".
 *
 * The followings are the available columns in table 'ScreenShot':
 * @property integer $screenshotid
 * @property string $fileName
 * @property string $fileMimeType
 * @property string $fileData
 * @property integer $uploader
 * @property string $dateUploaded
 * @property integer $bugid
 *
 * The followings are the available model relations:
 * @property Bug $bug
 * @property User $uploader0
 */
class ScreenShot extends CActiveRecord
{
	public $uploadedFile;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ScreenShot';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
    {
    	return array(
    		array('uploadedFile,', 'file', 
    			'safe' => true,
    			'types'=> 'jpg, jpeg, png, gif',
    			'maxSize' => (1024 * 1024 * 2), // 2mb
			),
            array('uploadedFile, bugid', 'safe'),
        );
    }
 
 
    /**
    * Saves the name, size, type and data of the uploaded file
    */
    public function beforeSave()
    {
        if($file=CUploadedFile::getInstance($this,'uploadedFile'))
        {
            $this->fileName=$file->name;
            $this->fileMimeType=$file->type;
            $this->fileData=file_get_contents($file->tempName);
            $this->uploader = yii::app()->user->getId();
        }
 
    return parent::beforeSave();
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bug' => array(self::BELONGS_TO, 'Bug', 'bugid'),
			'uploader0' => array(self::BELONGS_TO, 'User', 'uploader'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'screenshotid' => 'Screenshotid',
			'fileName' => 'File Name',
			'fileMimeType' => 'File Mime Type',
			'fileData' => 'File Data',
			'uploader' => 'Uplaoded By',
			'dateUploaded' => 'Date Uploaded',
			'bugid' => 'Bugid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('screenshotid',$this->screenshotid);
		$criteria->compare('fileName',$this->fileName,true);
		$criteria->compare('fileMimeType',$this->fileMimeType,true);
		$criteria->compare('fileData',$this->fileData,true);
		$criteria->compare('uploader',$this->uploader);
		$criteria->compare('dateUploaded',$this->dateUploaded,true);
		$criteria->compare('bugid',$this->bugid);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ScreenShot the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
