<?php
class BackupRecord extends CActiveRecord
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeSave()
    {
        if (!$this->isNewRecord) {
            $history = new BugRevision();
            $attributes = $this->getAttributes();

            Yii::log('Error while saving snapshot [' . print_r($attributes, true) . ']', CLogger::LEVEL_ERROR, 'application.models.History');

            $history->bugid = $attributes['bugid'];
            $history->bugTitle = $attributes['bugTitle'];
            $history->bugDescription = $attributes['bugDescription'];
            $history->active = $attributes['active'];
            $history->revisedBy = yii::app()->user->getid();
            $history->save();
        }

        return parent::beforeSave(); 
    }

    public function beforeDelete()
    {
        $history = new BugRevision();
        $history->snapshot($this->createSnapshot('delete'));

        return parent::beforeDelete();
    }
}
?>