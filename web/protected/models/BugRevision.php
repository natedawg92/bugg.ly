<?php

/**
 * This is the model class for table "BugRevision".
 *
 * The followings are the available columns in table 'BugRevision':
 * @property integer $bugid
 * @property string $revisionTime
 * @property string $BugTitle
 * @property string $bugDescription
 * @property integer $active
 * @property integer $revisedBy
 *
 * The followings are the available model relations:
 * @property Bug $bug
 * @property User $revisedBy0
 */
class BugRevision extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'BugRevision';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array();
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bug' => array(self::BELONGS_TO, 'Bug', 'bugid'),
			'revisedBy0' => array(self::BELONGS_TO, 'User', 'revisedBy'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bugid' => 'Bug ID',
			'revisionTime' => 'Revision Time',
			'bugTitle' => 'Bug Title',
			'bugDescription' => 'Bug Description',
			'active' => 'Active',
			'revisedBy' => 'Revised By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bugid',$this->bugid);
		$criteria->compare('revisionTime',$this->revisionTime,true);
		$criteria->compare('BugTitle',$this->BugTitle,true);
		$criteria->compare('bugDescription',$this->bugDescription,true);
		$criteria->compare('active',$this->active);
		$criteria->compare('revisedBy',$this->revisedBy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return BugRevision the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
