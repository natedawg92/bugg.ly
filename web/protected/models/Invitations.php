<?php

/**
 * This is the model class for table "Invitations".
 *
 * The followings are the available columns in table 'Invitations':
 * @property integer $invitationid
 * @property integer $sender
 * @property integer $invitee
 * @property integer $team
 * @property integer $completed
 * @property string $inviteeEmail
 * @property string $rString
 *
 * The followings are the available model relations:
 * @property Team $team0
 * @property User $sender0
 * @property User $invitee0
 */
class Invitations extends CActiveRecord
{

	public $inviteeName;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Invitations';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sender, team, rString', 'required'),
			array('sender, invitee, team, completed', 'numerical', 'integerOnly'=>true),
			array('inviteeEmail', 'length', 'max'=>100),
			array('rString', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('invitationid, sender, invitee, team, completed, inviteeEmail, rString', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'team0' => array(self::BELONGS_TO, 'Team', 'team'),
			'sender0' => array(self::BELONGS_TO, 'User', 'sender'),
			'invitee0' => array(self::BELONGS_TO, 'User', 'invitee'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'invitationid' => 'Invitationid',
			'sender' => 'Sender',
			'invitee' => 'Invitee',
			'team' => 'Team',
			'completed' => 'Completed',
			'inviteeEmail' => 'Invitee Email',
			'rString' => 'R String',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('invitationid',$this->invitationid);
		$criteria->compare('sender',$this->sender);
		$criteria->compare('invitee',$this->invitee);
		$criteria->compare('team',$this->team);
		$criteria->compare('completed',$this->completed);
		$criteria->compare('inviteeEmail',$this->inviteeEmail,true);
		$criteria->compare('rString',$this->rString,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Invitations the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
