<?php

/**
 * This is the model class for table "Project".
 *
 * The followings are the available columns in table 'Project':
 * @property integer $projectid
 * @property string $projectTitle
 * @property integer $projectOwner
 * @property integer $projectTeam
 * @property integer $complete
 *
 * The followings are the available model relations:
 * @property Bug[] $bugs
 * @property Team $projectTeam0
 * @property User $projectOwner0
 */
class Project extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Project';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('projectTitle, projectOwner, projectTeam', 'required'),
			array('projectOwner, projectTeam, active', 'numerical', 'integerOnly'=>true),
			array('projectTitle', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('projectid, projectTitle, projectOwner, projectTeam, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'bugs' => array(self::HAS_MANY, 'Bug', 'projectid'),
			'projectTeamTeam' => array(self::BELONGS_TO, 'Team', 'projectTeam'),
			'owner' => array(self::BELONGS_TO, 'User', 'projectOwner'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'projectid' => 'Project ID',
			'projectTitle' => 'Project Title',
			'projectOwner' => 'Project Owner',
			'projectTeam' => 'Project Team',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('projectid',$this->projectid);
		$criteria->compare('projectTitle',$this->projectTitle,true);
		$criteria->compare('projectOwner',$this->projectOwner);
		$criteria->compare('projectTeam',$this->projectTeam);
		$criteria->compare('complete',$this->complete);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
