<?php

class InviteCode extends CFormModel{

	public $code;

	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('code', 'required'),
			array('code', 'safe'),
			array('code', 'length', 'is' => 20),
		);
	}

	public function attributeLAbels(){
		return array(
				'code' => 'Invitation Code',
			);
	}
}
?>