<?php

/**
 * This is the model class for table "Bug".
 *
 * The followings are the available columns in table 'Bug':
 * @property integer $bugid
 * @property string $bugTitle
 * @property string $bugDescription
 * @property integer $projectid
 * @property integer $active
 *
 * The followings are the available model relations:
 * @property Project $project
 * @property BugRevision[] $bugRevisions
 * @property ScreenShot[] $screenShots
 */
class Bug extends BackupRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Bug';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('bugTitle, bugDescription, projectid', 'required'),
			array('projectid, active', 'numerical', 'integerOnly'=>true),
			array('bugTitle', 'length', 'max'=>45),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('bugid, bugTitle, bugDescription, projectid, active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'project' => array(self::BELONGS_TO, 'Project', 'projectid'),
			'bugRevisions' => array(self::HAS_MANY, 'BugRevision', 'bugid'),
			'screenShots' => array(self::HAS_MANY, 'ScreenShot', 'bugid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'bugid' => 'Bugid',
			'bugTitle' => 'Bug Name',
			'bugDescription' => 'Description of Bug',
			'projectid' => 'Projectid',
			'active' => 'Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('bugid',$this->bugid);
		$criteria->compare('bugTitle',$this->bugTitle,true);
		$criteria->compare('bugDescription',$this->bugDescription,true);
		$criteria->compare('projectid',$this->projectid);
		$criteria->compare('active',$this->active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Bug the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
