<?php
/* @var $this ProjectController */
/* @var $model Project */
/* @var $form CActiveForm */

$listData = CHtml::listData($teamData, 'teamid', 'teamName');

$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'project-form',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);
 
echo $form->textFieldGroup($model, 'projectTitle');
echo $form->dropDownListGroup($model,'projectTeam', array('widgetOptions' => array('data' => $listData,'htmlOptions' => array('empty'=>'Select a Team'),)));
echo $form->switchGroup($model, 'active');

$this->widget(
'booster.widgets.TbButton',
array('buttonType' => 'submit', 'label' => 'Submit Project')
);
 
$this->endWidget();
unset($form);