<?php
/* @var $this ProjectController */
/* @var $data Project */
$owner = User::model()->findByPk($data->projectOwner);
$team = Team::model()->findByPk($data->projectTeam);
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('projectTitle')); ?>:</b>
	<?php echo CHtml::Link($data->projectTitle, array('view', 'id'=>$data->projectid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('projectOwner')); ?>:</b>
	<?php echo CHtml::encode($owner->firstName.' '.$owner->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('projectTeam')); ?>:</b>
	<?php echo CHtml::encode($team->teamName); ?>
	<br />

	<b><?php echo CHtml::encode('# of active Bugs'); ?>:</b>
	<?php echo CHtml::encode(Bug::model()->countByAttributes(array('projectid'=>$data->projectid, 'active'=>true))); ?>
	<br />


</div>