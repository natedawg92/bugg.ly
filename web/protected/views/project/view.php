<?php
/* @var $this ProjectController */
/* @var $model Project */

$this->breadcrumbs=array(
	'Projects'=>array('index'),
	$model->projectTitle,
);


$this->menu=array(
	array('label'=>'List Project', 'url'=>array('index')),
	array('label'=>'Create Project', 'url'=>array('create')),
	array('label'=>'Update Project', 'url'=>array('update', 'id'=>$model->projectid)),
	array('label'=>'Delete Project', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->projectid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Project', 'url'=>array('admin')),
	array('label'=>'Add a new Bug', 'url'=>array('bug/create', 'projectid'=>$model->projectid)),
);
?>

<h1>View Project <?php echo $model->projectTitle; ?></h1>

<?php 
	$this->widget('zii.widgets.CDetailView', array(
		'data'=>$model,
		'attributes'=>array(
			'projectTitle',
			array(
				'name'=>'Project Owner',
				'type'=>'html',
				'value'=>CHtml::link($user->firstName." ".$user->lastName, array("user/view", "id"=>$user->userid)),
			),
			array(
				'name'=>'Project Team',
				'type'=>'html',
				'value'=>CHtml::link($team->teamName, array("team/view", "id"=>$team->teamid)),
			),
			array(
				'name'=>'Active Bugs',
				'type'=>'html',
				'value'=>implode('<br/>', $activeBugLinks),
			),
			array(
				'name'=>'Completed Bugs',
				'type'=>'html',
				'value'=>implode('<br/>', $completeBugLinks),
			),
		),
	));
?>
