<?php
/* @var $this BugController */
/* @var $model Bug */

$project = Project::model()->findByPk($model->projectid);
$team = Team::model()->findByPk($project->projectTeam);

$this->breadcrumbs=array(
	$project->projectTitle =>array('project/view', 'id'=>$model->projectid),
	$model->bugTitle,
);

$this->menu=array(
	array('label'=>'List Bug', 'url'=>array('index')),
	array('label'=>'Create Bug', 'url'=>array('create')),
	array('label'=>'Update Bug', 'url'=>array('update', 'id'=>$model->bugid)),
	array('label'=>'Delete Bug', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->bugid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Bug', 'url'=>array('admin')),
	array('label'=>'Upload A Screenshot', 'url'=>array('screenShot/create', 'bugid'=>$model->bugid)),
);

echo "<h1>View Bug: $model->bugTitle</h1>";

$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bugTitle',
		'bugDescription',
		array(
			'name'=>'Project',
			'type'=>'html',
			'value'=>CHtml::link($project->projectTitle, array('project/view', 'id'=>$model->projectid)),
		),
		array(
			'name'=>'Team',
			'type'=>'html',
			'value'=>CHtml::link($team->teamName, array('team/view', 'id'=>$team->teamid)),
		),
	),
)); 

echo "<h2>Bug Revisions</h2>";
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'emptyText'=>'There has Been no chnages to this bug submission',
	'columns'=>array(
		array(            // display 'create_time' using an expression
			'name'=>'Revision Time',
			'value'=>'date("M j Y g:i A", strtotime($data->revisionTime));',
		),
		array(
			'name'=>'Bug Title',
			'value'=>'$data->bugTitle',
		),
		array(
			'name'=>'Bug Description',
			'value'=>'$data->bugDescription'
		),
		array(
			'name'=>'Active',
			'value'=>'$data->active==0? "false" : "true"',
		),
		array(
			'name'=>'Edited By',
			'type'=>'html',
			'value'=>'CHtml::link(User::model()->findByPk($data->revisedBy)->userName, array("user/view", "id"=>$data->revisedBy))',
		),
	),
));


echo "<h2>Screenshots</h2>";

foreach ($screenshots as $s){
	echo CHtml::image(
		$this->createAbsoluteUrl('screenShot/display', array('id'=>$s->screenshotid)), 
		'Screenshot of '.$model->bugTitle,
		array(
			'width'=>'100%',
			'max-height'=>'100%',
			'title'=>'image title here',
			'data-toggle' => 'modal',
			'data-target' => '#imageModal'.$s->screenshotid,
		) 
	);
	echo "<br/>Uploaded by ".User::model()->findByPk($s->uploader)->userName."<br/>";

$this->beginWidget(
	'booster.widgets.TbModal',
	array(
		'id' => 'imageModal'.$s->screenshotid,
		'htmlOptions' => array(
			'width'=> '100%',
		)
		)
	); 
?> 
<div>
	<a class="close" data-dismiss="modal">&times;</a>
	<?php
		echo CHtml::image(
			$this->createAbsoluteUrl('screenShot/display', array('id'=>$s->screenshotid)), 
			'Screenshot of '.$model->bugTitle,
			array(
				'title'=>'image title here',
				'width'=>'100%',
			) 
		);
	?>
</div>
 
<?php $this->endWidget();
}
?>
