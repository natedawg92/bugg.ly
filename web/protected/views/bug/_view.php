<?php
/* @var $this BugController */
/* @var $data Bug */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bugid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bugid), array('view', 'id'=>$data->bugid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bugTitle')); ?>:</b>
	<?php echo CHtml::encode($data->bugTitle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bugDescription')); ?>:</b>
	<?php echo CHtml::encode($data->bugDescription); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('projectid')); ?>:</b>
	<?php echo CHtml::encode($data->projectid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active')); ?>:</b>
	<?php echo CHtml::encode($data->active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('screenshotid')); ?>:</b>
	<?php echo CHtml::encode($data->screenshotid); ?>
	<br />


</div>