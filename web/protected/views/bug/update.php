<?php
/* @var $this BugController */
/* @var $model Bug */

$this->breadcrumbs=array(
	'Bugs'=>array('index'),
	$model->bugid=>array('view','id'=>$model->bugid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Bug', 'url'=>array('index')),
	array('label'=>'Create Bug', 'url'=>array('create')),
	array('label'=>'View Bug', 'url'=>array('view', 'id'=>$model->bugid)),
	array('label'=>'Manage Bug', 'url'=>array('admin')),
);
?>

<h1>Update Bug <?php echo $model->bugid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'listData'=>$listData)); ?>