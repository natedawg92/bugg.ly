<?php
/* @var $this BugController */
/* @var $model Bug */
/* @var $form TbActiveForm */

$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'bug-form',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);
 
echo $form->textFieldGroup($model, 'bugTitle');
if(!isset($_GET['projectid'])){
	echo $form->dropDownListGroup($model, 'projectid', array('widgetOptions'=>array('data'=>$listData)));
}else{
	echo $form->hiddenField($model, 'projectid', array('value'=>$_GET['projectid']));
}
echo $form->textAreaGroup($model,'bugDescription');
echo $form->switchGroup($model, 'active');

$this->widget(
'booster.widgets.TbButton',
array('buttonType' => 'submit', 'label' => 'submit bug')
);
 
$this->endWidget();
unset($form);
?>