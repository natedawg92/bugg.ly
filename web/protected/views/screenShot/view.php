<?php
/* @var $this ScreenShotController */
/* @var $model ScreenShot */

$this->breadcrumbs=array(
	'Screen Shots'=>array('index'),
	$model->screenshotid,
);

$this->menu=array(
	array('label'=>'List ScreenShot', 'url'=>array('index')),
	array('label'=>'Create ScreenShot', 'url'=>array('create')),
	array('label'=>'Update ScreenShot', 'url'=>array('update', 'id'=>$model->screenshotid)),
	array('label'=>'Delete ScreenShot', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->screenshotid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ScreenShot', 'url'=>array('admin')),
);
?>

<h1>View ScreenShot #<?php echo $model->screenshotid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'screenshotid',
		'fileName',
		'fileMimeType',
		'fileData',
		'uploader',
		'dateUploaded',
	),
)); ?>
