<?php
/* @var $this ScreenShotController */
/* @var $model ScreenShot */

$this->breadcrumbs=array(
	'Screen Shots'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ScreenShot', 'url'=>array('index')),
	array('label'=>'Manage ScreenShot', 'url'=>array('admin')),
);
?>

<h1>Create ScreenShot</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'listData'=>$listData)); ?>