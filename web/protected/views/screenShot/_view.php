<?php
/* @var $this ScreenShotController */
/* @var $data ScreenShot */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('screenshotid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->screenshotid), array('view', 'id'=>$data->screenshotid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fileName')); ?>:</b>
	<?php echo CHtml::encode($data->fileName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fileMimeType')); ?>:</b>
	<?php echo CHtml::encode($data->fileMimeType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fileData')); ?>:</b>
	<?php echo CHtml::encode($data->fileData); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('uploader')); ?>:</b>
	<?php echo CHtml::encode($data->uploader); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dateUploaded')); ?>:</b>
	<?php echo CHtml::encode($data->dateUploaded); ?>
	<br />


</div>