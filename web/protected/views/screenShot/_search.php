<?php
/* @var $this ScreenShotController */
/* @var $model ScreenShot */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'screenshotid'); ?>
		<?php echo $form->textField($model,'screenshotid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fileName'); ?>
		<?php echo $form->textField($model,'fileName',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fileMimeType'); ?>
		<?php echo $form->textField($model,'fileMimeType',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'fileData'); ?>
		<?php echo $form->textField($model,'fileData'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'uploader'); ?>
		<?php echo $form->textField($model,'uploader'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dateUploaded'); ?>
		<?php echo $form->textField($model,'dateUploaded'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->