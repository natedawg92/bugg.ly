<?php
/* @var $this ScreenShotController */
/* @var $model ScreenShot */

$this->breadcrumbs=array(
	'Screen Shots'=>array('index'),
	$model->screenshotid=>array('view','id'=>$model->screenshotid),
	'Update',
);

$this->menu=array(
	array('label'=>'List ScreenShot', 'url'=>array('index')),
	array('label'=>'Create ScreenShot', 'url'=>array('create')),
	array('label'=>'View ScreenShot', 'url'=>array('view', 'id'=>$model->screenshotid)),
	array('label'=>'Manage ScreenShot', 'url'=>array('admin')),
);
?>

<h1>Update ScreenShot <?php echo $model->screenshotid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'listData'=>$listData)); ?>