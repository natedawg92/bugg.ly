<?php
/* @var $this ScreenShotController */
/* @var $model ScreenShot */
/* @var $form TbActiveForm */

$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'screenshot-form',
		'htmlOptions' => array('class' => 'well', 'enctype' => 'multipart/form-data'), // for inset effect
	)
);
 
 if(!isset($_GET['bugid'])){
 	echo $form->dropDownListGroup($model,'bugid', array('widgetOptions' => array('data' => $listData,'htmlOptions' => array('empty'=>'Select a Bug'),)));
 }else{
 	echo $form->hiddenField($model, 'bugid', array('value'=>$_GET['bugid']));
 }

echo $form->fileFieldGroup($model, 'uploadedFile');

$this->widget(
'booster.widgets.TbButton',
array('buttonType' => 'submit', 'label' => 'Upload new screenshot')
);
 
$this->endWidget();