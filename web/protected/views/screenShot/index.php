<?php
/* @var $this ScreenShotController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Screen Shots',
);

$this->menu=array(
	array('label'=>'Create ScreenShot', 'url'=>array('create')),
	array('label'=>'Manage ScreenShot', 'url'=>array('admin')),
);
?>

<h1>Screen Shots</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
