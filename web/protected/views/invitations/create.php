<?php
/* @var $this InvitationsController */
/* @var $model Invitations */

$this->breadcrumbs=array(
	'Invitations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Invitations', 'url'=>array('index')),
	array('label'=>'Manage Invitations', 'url'=>array('admin')),
);
?>

<h1>Create Invitations</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>