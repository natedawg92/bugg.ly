<?php
/* @var $this InvitationsController */
/* @var $model Invitations */

$this->breadcrumbs=array(
	'Invitations'=>array('index'),
	$model->invitationid,
);

$this->menu=array(
	array('label'=>'List Invitations', 'url'=>array('index')),
	array('label'=>'Create Invitations', 'url'=>array('create')),
	array('label'=>'Update Invitations', 'url'=>array('update', 'id'=>$model->invitationid)),
	array('label'=>'Delete Invitations', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->invitationid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Invitations', 'url'=>array('admin')),
);
?>

<h1>View Invitations #<?php echo $model->invitationid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'invitationid',
		'sender',
		'invitee',
		'team',
		'accepted',
		'inviteeEmail',
	),
)); ?>
