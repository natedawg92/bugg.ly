<?php
/** @var TbActiveForm $form */
$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'accept-form',
		'type' => 'vertical',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);
echo "<h3>Invitation to join $team->teamName</h3>";
 
echo $form->checkboxGroup($model, 'accept');

$this->widget(
	'booster.widgets.TbButton',
	array('buttonType' => 'submit', 'label' => 'submit',)
);
 
$this->endWidget();
unset($form);
?>