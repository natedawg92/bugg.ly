<?php
/** @var $model InviteCode */
/** @var $this InvitationsController */
/** @var $form TbActiveForm */
$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'InviteCode',
		'type' => 'vertical',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);
echo "<h3>Enter You Invitation Code here</h3>";
 
echo $form->textFieldGroup($model, 'code');

$this->widget(
    'booster.widgets.TbButton',
    array('buttonType' => 'submit', 'label' => 'Submit')
);
$this->endWidget();
?>