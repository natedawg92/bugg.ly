<?php
/* @var $this InvitationsController */
/* @var $data Invitations */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('invitationid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->invitationid), array('view', 'id'=>$data->invitationid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sender')); ?>:</b>
	<?php echo CHtml::encode($data->sender); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('invitee')); ?>:</b>
	<?php echo CHtml::encode($data->invitee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('team')); ?>:</b>
	<?php echo CHtml::encode($data->team); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('completed')); ?>:</b>
	<?php echo CHtml::encode($data->completed); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('inviteeEmail')); ?>:</b>
	<?php echo CHtml::encode($data->inviteeEmail); ?>
	<br />


</div>