<?php
/* @var $this InvitationsController */
/* @var $model Invitations */

$this->breadcrumbs=array(
	'Invitations'=>array('index'),
	$model->invitationid=>array('view','id'=>$model->invitationid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Invitations', 'url'=>array('index')),
	array('label'=>'Create Invitations', 'url'=>array('create')),
	array('label'=>'View Invitations', 'url'=>array('view', 'id'=>$model->invitationid)),
	array('label'=>'Manage Invitations', 'url'=>array('admin')),
);
?>

<h1>Update Invitations <?php echo $model->invitationid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>