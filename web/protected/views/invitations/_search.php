<?php
/* @var $this InvitationsController */
/* @var $model Invitations */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'invitationid'); ?>
		<?php echo $form->textField($model,'invitationid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sender'); ?>
		<?php echo $form->textField($model,'sender'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'invitee'); ?>
		<?php echo $form->textField($model,'invitee'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'team'); ?>
		<?php echo $form->textField($model,'team'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'accepted'); ?>
		<?php echo $form->textField($model,'accepted'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'inviteeEmail'); ?>
		<?php echo $form->textField($model,'inviteeEmail',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->