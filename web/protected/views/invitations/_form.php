<?php
/* @var $this InvitationsController */
/* @var $model Invitations */
/* @var $form TbActiveForm */

$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'invitations-form',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);

echo $form->textFieldGroup($model, 'inviteeName');  
echo $form->textFieldGroup($model, 'inviteeEmail');

$this->widget(
	'booster.widgets.TbButton',
	array('buttonType' => 'submit', 'label' => 'Send Invitation')
);
     
$this->endWidget();