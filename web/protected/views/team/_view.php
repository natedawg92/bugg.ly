<?php
/* @var $this TeamController */
/* @var $data Team */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('teamid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->teamid), array('view', 'id'=>$data->teamid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('teamName')); ?>:</b>
	<?php echo CHtml::encode($data->teamName); ?>
	<br />


</div>