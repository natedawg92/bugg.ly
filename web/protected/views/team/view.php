<?php
/* @var $this TeamController */
/* @var $model Team */

$this->breadcrumbs=array(
	'Teams'=>array('index'),
	$model->teamName,
);

$this->menu=array(
	array('label'=>'List Team', 'url'=>array('index')),
	array('label'=>'Create Team', 'url'=>array('create')),
	array('label'=>'Update Team', 'url'=>array('update', 'id'=>$model->teamid)),
	array('label'=>'Delete Team', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->teamid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Team', 'url'=>array('admin')),
	array('label'=>'Invite New Member', 'url'=>array('/invitations/create/'.$model->teamid)),
);
?>

<h1>Team: <?php echo $model->teamName; ?></h1>
<h2>Members:</h2>
<?php 
	if(!empty($members)){
		foreach($members as $m){
			echo CHtml::link($m->firstName.' '.$m->lastName, array('user/view/', 'id'=>$m->userid))."<br/>";
		}
	}
?>
