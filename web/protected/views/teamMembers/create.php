<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'teamMemberss'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List teamMemberss', 'url'=>array('index')),
    array('label'=>'Manage teamMembers', 'url'=>array('admin')),
);
?>

<h1>Create teamMembers</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
