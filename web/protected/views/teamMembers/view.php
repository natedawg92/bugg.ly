<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'teamMemberss'=>array('index'),
	'View',
);

$this->menu=array(
	array('label'=>'List teamMembers', 'url'=>array('index')),
	array('label'=>'Create teamMembers', 'url'=>array('create')),
	array('label'=>'Update teamMembers', 'url'=>array('update', 'userid'=>$model->userid, 'teamid'=>$model->teamid)),
	array('label'=>'Delete teamMembers', 'url'=>'delete', 
	      'linkOptions'=>array('submit'=>array('delete',
	                                           'userid'=>$model->userid, 'teamid'=>$model->teamid),
									'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage teamMembers', 'url'=>array('admin')),
);
?>

<h1>View teamMembers</h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'userid',
		'teamid',
	),
)); ?>
