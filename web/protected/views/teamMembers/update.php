<!--Generated using Gimme CRUD freeware from www.HandsOnCoding.net -->
<?php
$this->breadcrumbs=array(
	'teamMemberss'=>array('index'),
	'View'=>array('view', 'userid'=>$model->userid, 'teamid'=>$model->teamid),
	'Update',
);

$this->menu=array(
	array('label'=>'List teamMembers', 'url'=>array('index')),
	array('label'=>'Create teamMembers', 'url'=>array('create')),
	array('label'=>'View teamMembers', 'url'=>array('view', 'userid'=>$model->userid, 'teamid'=>$model->teamid)),
	array('label'=>'Manage teamMembers', 'url'=>array('admin')),
); 
?>

<h1>Update Client</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
