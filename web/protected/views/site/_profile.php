<?php
	$teamNames = array();
	foreach ($teams as $t){
		array_push($teamNames, CHtml::link($t->teamName, array('team/view/', 'id'=>$t->teamid)));
	}

	$this->widget(
		'booster.widgets.TbDetailView',
		array(
			'data' => $user,
			'attributes' => array(
				array('name' => 'userName', 'label' => 'Username'),
				array('name' => 'firstName', 'label' => 'First name'),
				array('name' => 'lastName', 'label' => 'Last name'),
				array('value' => implode('<br/>', $teamNames), 'label' => 'Teams', 'type'=>'html'),
			),
		)
	);
?>