<?php
	echo "<h3>My Invitations</h3>";  

	$dataProvider=new CArrayDataProvider(
		$invites, 
		array(
		'id'=>'invites', //this is an identifier for the array data provider
		'sort'=>false,
		'keyField'=>'invitationid', //this is what will be considered your key field
		'pagination'=>array(
			'pageSize'=>10, //eureka! you can configure your pagination from here
			),
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
    'dataProvider'=>$dataProvider,
    'emptyText'=>'You have no incomplete invitations',
    'columns'=>array(
        array(            // display 'create_time' using an expression
            'name'=>'Team Name',
            'value'=>'Team::model()->findByPk($data->team)->teamName',
        ),
        array(            // display 'create_time' using an expression
            'name'=>'Invitation Code',
            'value'=>'$data->rString',
        ),
        array(
        	'name'=>'Accept',
        	'value'=>'CHtml::Link("accept", array("invitations/accept", "rString"=>$data->rString))',
        	'type'=>'html',
        ),
        array(
        	'name'=>'Decline',
        	'value'=>'CHtml::Link("decline", array("invitations/decline", array("rString"=>$data->rString)))',
        	'type'=>'html',
        ),
    ),
));
?>