<?php
	/* @var $this UserController */
	/* @var $model User */
	$this->breadcrumbs=array('Dashboard');
?>

<div class="col-xs-12 col-md-6">
	<?php $this->widget(
		'booster.widgets.TbTabs',
		array(
			'type' => 'tabs',
			'placement' => 'left',
			'tabs' => $tabs,
		)
	);?>
</div>
