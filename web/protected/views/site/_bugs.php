<?php
	echo "<h3>Bugs</h3>";

	$dataProvider=new CArrayDataProvider(
		$bugs, 
		array(
		'id'=>'bugs', //this is an identifier for the array data provider
		'sort'=>false,
		'keyField'=>'bugid', //this is what will be considered your key field
		'pagination'=>array(
			'pageSize'=>10, //eureka! you can configure your pagination from here
			),
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'emptyText'=>'There are currently no bugs with the projects you are working on',
		'columns'=>array(
			array(            // display 'create_time' using an expression
				'name'=>'Bug Title',
				'value'=>'$data->bugTitle',
			),
			array(
				'name'=>'Project',
				'type'=>'html',
				'value'=>'CHtml::link(Project::model()->findByPk($data->projectid)->projectTitle, array("project/view", "id"=>$data->projectid))',
			),
			array(
				'name'=>'Team',
				'type'=>'html',
				'value'=>'CHtml::link(Team::model()->findByPk(Project::model()->findByPk($data->projectid)->projectTeam)->teamName, array("team/view", "id"=>Project::model()->findByPk($data->projectid)->projectTeam))',
			),
			array(
				'name'=>'View',
				'type'=>'html',
				'value'=>'CHtml::link("View", array("bug/view", "id"=>$data->bugid))',
				),
		),
	));
?>