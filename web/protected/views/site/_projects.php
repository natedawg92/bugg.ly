<?php
	echo "<h3>Projects that i am involved with</h3>";

	$dataProvider=new CArrayDataProvider(
		$projects, 
		array(
		'id'=>'projects', //this is an identifier for the array data provider
		'sort'=>false,
		'keyField'=>'projectid', //this is what will be considered your key field
		'pagination'=>array(
			'pageSize'=>10, //eureka! you can configure your pagination from here
			),
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'emptyText'=>'You have no active projects',
		'summaryText' => '',
		'columns'=>array(
			array(            // display 'create_time' using an expression
				'name'=>'Project Tile',
				'value'=>'$data->projectTitle',
			),
			array(
				'name'=>'Team Involved',
				'value'=>'Team::model()->findByPk($data->projectTeam)->teamName',
			),
			array(
				'name'=>'# of Bugs',
				'value'=>'Bug::model()->countByAttributes(array("projectid"=>$data->projectid))'
			),
			array(
				'name'=>'View',
				'type'=>'html',
				'value'=>'CHtml::link("View", array("project/view", "id"=>$data->projectid))',
			),
		),
	));

	echo "<h3>Completed Projects</h3>";
	$cDataProvider=new CArrayDataProvider(
		$completeProjects, 
		array(
			'id'=>'projects', //this is an identifier for the array data provider
			'sort'=>false,
			'keyField'=>'projectid', //this is what will be considered your key field
			'pagination'=>array(
				'pageSize'=>10, //eureka! you can configure your pagination from here
			),
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$cDataProvider,
		'emptyText'=>'You have not been involved with any completed projects',
		'summaryText' => '',
		'columns'=>array(
			array(            // display 'create_time' using an expression
				'name'=>'Project Tile',
				'value'=>'$data->projectTitle',
			),
			array(
				'name'=>'Team Involved',
				'value'=>'Team::model()->findByPk($data->projectTeam)->teamName',
			),
			array(
				'name'=>'# of Bugs',
				'value'=>'Bug::model()->countByAttributes(array("projectid"=>$data->projectid))'
			),
			array(
				'name'=>'View',
				'type'=>'html',
				'value'=>'CHtml::link("View", array("project/view", "id"=>$data->projectid))',
			),
		),
	));
?>