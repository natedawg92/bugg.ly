<?php
	echo "<h3>My Teams</h3>";

	$dataProvider=new CArrayDataProvider(
		$teams, 
		array(
		'id'=>'teams', //this is an identifier for the array data provider
		'sort'=>false,
		'keyField'=>'teamid', //this is what will be considered your key field
		'pagination'=>array(
			'pageSize'=>10, //eureka! you can configure your pagination from here
			),
		)
	);

	$this->widget('zii.widgets.grid.CGridView', array(
		'dataProvider'=>$dataProvider,
		'emptyText'=>'You have not currently involved with any teams',
		'columns'=>array(
			array(            // display 'create_time' using an expression
				'name'=>'Team Name',
				'value'=>'$data->teamName',
			),
			array(
				'name'=>'# of Members',
				'value'=>'TeamMembers::model()->countByAttributes(array("teamid"=>$data->teamid))',
			),
			array(
				'name'=>'View',
				'type'=>'html',
				'value'=>'CHtml::link("View", array("team/view", "id"=>$data->teamid))',
				),
		),
	));

?>