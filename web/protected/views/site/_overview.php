<div class="span-23 showgrid">
    <div class="dashboardIcons span-16">
        <div class="dashIcon span-3">
            <?php echo CHtml::link(CHtml::Image(Yii::app()->theme->baseUrl.'/images/big_icons/project.png', 'New Project'), array('project/create')); ?>
            <div class="dashIconText"><?php echo CHtml::link('New Project', array('project/create')); ?></div>
        </div>
        
        <div class="dashIcon span-3">
            <?php echo CHtml::link(CHtml::Image(Yii::app()->theme->baseUrl.'/images/big_icons/bug.png', 'New Bug'), array('bug/create')); ?>
            <div class="dashIconText"><?php echo CHtml::link('New Bug', array('bug/create')); ?></div>
        </div>
        
        <div class="dashIcon span-3">
            <?php echo CHtml::link(CHtml::Image(Yii::app()->theme->baseUrl.'/images/big_icons/team.png', 'New Team'), array('team/create')); ?>
            <div class="dashIconText"><?php echo CHtml::link('New Team', array('team/create')); ?></div>
        </div>
    </div>
</div>