<?php
/* @var $this AvatarController */
/* @var $model Avatar */

$this->breadcrumbs=array(
	'Avatars'=>array('index'),
	$model->avatarid=>array('view','id'=>$model->avatarid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Avatar', 'url'=>array('index')),
	array('label'=>'Create Avatar', 'url'=>array('create')),
	array('label'=>'View Avatar', 'url'=>array('view', 'id'=>$model->avatarid)),
	array('label'=>'Manage Avatar', 'url'=>array('admin')),
);
?>

<h1>Update Avatar <?php echo $model->avatarid; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>