<?php
/* @var $this AvatarController */
/* @var $model Avatar */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'avatar-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'avatarMimeType'); ?>
		<?php echo $form->textField($model,'avatarMimeType',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'avatarMimeType'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'avatarData'); ?>
		<?php echo $form->textField($model,'avatarData'); ?>
		<?php echo $form->error($model,'avatarData'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'public'); ?>
		<?php echo $form->textField($model,'public'); ?>
		<?php echo $form->error($model,'public'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->