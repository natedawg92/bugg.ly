<?php
/* @var $this AvatarController */
/* @var $data Avatar */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('avatarid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->avatarid), array('view', 'id'=>$data->avatarid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avatarMimeType')); ?>:</b>
	<?php echo CHtml::encode($data->avatarMimeType); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('avatarData')); ?>:</b>
	<?php echo CHtml::encode($data->avatarData); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('public')); ?>:</b>
	<?php echo CHtml::encode($data->public); ?>
	<br />


</div>