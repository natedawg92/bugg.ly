<?php
/* @var $this AvatarController */
/* @var $model Avatar */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'avatarid'); ?>
		<?php echo $form->textField($model,'avatarid'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'avatarMimeType'); ?>
		<?php echo $form->textField($model,'avatarMimeType',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'avatarData'); ?>
		<?php echo $form->textField($model,'avatarData'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'public'); ?>
		<?php echo $form->textField($model,'public'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->