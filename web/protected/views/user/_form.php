<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
/** @var TbActiveForm $form */
$form = $this->beginWidget(
	'booster.widgets.TbActiveForm',
	array(
		'id' => 'user-form',
		'htmlOptions' => array('class' => 'well'), // for inset effect
	)
);

echo $form->textFieldGroup($model, 'userName');
echo $form->textFieldGroup($model, 'firstName');
echo $form->textFieldGroup($model, 'lastName');
echo $form->textFieldGroup($model, 'email');

echo $form->passwordFieldGroup($model, 'password1');
echo $form->passwordFieldGroup($model, 'password_repeat');

$this->widget(
	'booster.widgets.TbButton',
	array('buttonType' => 'submit', 'label' => 'Register')
	);

$this->endWidget();
unset($form);