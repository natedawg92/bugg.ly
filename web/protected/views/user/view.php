<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->userName,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->userid)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->userid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User: <?php echo $model->userName; ?></h1>

<?php 
$teamNames = array();
foreach ($teams as $t){
	array_push($teamNames, CHtml::link($t->teamName, array('team/view/', 'id'=>$t->teamid)));
}
$this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'firstName',
		'lastName',
		array(
			'label'=>'Teams',
			'type'=>'html',
			'value'=>implode('<br/>', $teamNames),
		)
	),
)); 
?>
