<?php

class SiteController extends Controller
{

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
		);
	}

	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index');
	}

	public function actionDashboard()
	{
		//Generate User and team Info
		$user = User::model()->findByPk(array('userid' => yii::app()->user->getid()));
		$members = TeamMembers::model()->findAllByAttributes(array('userid'=>$user->userid));
		$teams = array();
		foreach ($members as $m){
			array_push($teams, Team::model()->findByPk($m->teamid));
		}

		//Generate Projects Information
		//Active Projects
		$projects = array();
		if(!empty($teams)){
			foreach ($teams as $t){
				$projects = Project::model()->findAllByAttributes(array('projectTeam'=>$t->teamid, 'active'=>true));
			}
		}
		$userProjects = Project::model()->findAllByAttributes(array('projectOwner'=>$user->userid, 'active'=>true));
		foreach ($userProjects as $up){
			$match = false;
			foreach($projects as $p){
				if ($up->projectid == $p->projectid){
					$match = true;
				}
			}
			if (!$match){
				array_push($projects, $up);
			}
		}
		//Copmpleted Projects
		$completeProjects = Project::model()->findAllByAttributes(array('projectTeam'=>$t->teamid, 'active'=>false));
		$completeUP = Project::model()->findAllByAttributes(array('projectOwner'=>$user->userid, 'active'=>false));
		foreach ($completeUP as $cup){
			$match = false;
			foreach($completeProjects as $cp){
				if ($cup->projectid == $cp->projectid){
					$match = true;
				}
			}
			if (!$match){
				array_push($completeProjects, $cup);
			}
		}

		//Generate Bug Info
		$bugs = array();
		if(!empty($projects)){
			foreach($projects as $p){
				$bugs = Bug::model()->findAllByAttributes(array('projectid'=>$p->projectid, 'active'=>true));
			}
		}

		//Generate Invite Info
		$invites = array();
		$invitations = Invitations::model()->findAllByAttributes(array('invitee'=>yii::app()->user->getId(), 'completed'=>false));
		foreach ($invitations as $inv){
			if($inv->completed === 0){
				array_push($invites, $inv);
			}
		}
		$userInvites = Invitations::model()->findAllByAttributes(array('inviteeEmail'=>$user->email, 'completed'=>false));
		if(isset($userInvites)){
			foreach($userInvites as $ui){
				$match = false;
				foreach($invites as $i){
					if($i->invitationid === $ui->invitationid){
						$match = true;
					}
				}
				if(!$match){
					array_push($invites, $ui);
				}
			}
		}


		//Generate Tabs
		$tabs = array();
		array_push($tabs,array('label'=>'Dashboard','content'=>$this->renderPartial('_overview', array(), true),'active'=>true)); //Dashboard Overview Tab
		array_push($tabs,array('label'=>'Profile','content'=>$this->renderPartial('_profile', array('user'=>$user, 'teams'=>$teams), true),'active' => false,)); //User Profile Tab
		array_push($tabs,array('label'=>'Projects','content'=>$this->renderPartial('_projects', array('projects'=>$projects, 'completeProjects'=>$completeProjects), true),'active'=> false,)); //Projects Tab
		array_push($tabs,array('label'=>'Bugs','content'=>$this->renderPartial('_bugs', array('bugs'=>$bugs), true),'active'=> false,)); //Bugs Tab
		array_push($tabs,array('label'=>'Teams','content'=>$this->renderPartial('_teams', array('teams'=>$teams), true),'active'=> false,)); //Teams Tab
		array_push($tabs,array('label'=>'Invites','content'=>$this->renderPartial('_invites', array('invites'=>$invites), true), 'active'=>false,));

		$this->render('dashboard', array('tabs'=>$tabs));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-Type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;

		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login()){
				if(Yii::app()->user->returnUrl != Yii::app()->BaseUrl.'/index.php'){
					$this->redirect(Yii::app()->user->returnUrl);
				} else {
					$this->redirect('dashboard');
				}
			}	
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}