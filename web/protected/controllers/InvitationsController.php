<?php

class InvitationsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'roles'=>array('guest'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view', 'create','update', 'accept', 'decline', 'inviteCode'),
				'roles'=>array('authenticated'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionAccept($rString){
		$invite = Invitations::model()->findByAttributes(array('rString'=>$rString));
		if(isset($invite) && $invite->invitee === yii::app()->user->getId()){
			$membership = new TeamMembers;
			$membership->userid = yii::app()->user->getId();
			$membership->teamid = $invite->team;
			$invite->completed = 1;
			if($membership->save() && $invite->save()){
				$this->redirect(Yii::app()->request->urlReferrer);
			}
		}elseif (!isset($invite)) {
			throw new CHttpException(400,'The specified invitation code cannot be found.');
		} elseif (!yii::app()->user->getId() === $invite->invitee) {
			throw new CHttpException(400,'The specified invitation code was not sent to you');
		}
	}

	public function actionDecline($rString){
		$invite = Invitations::model()->findByAttributes(array('rString'=>$rString));
		if(isset($invite) && $invite->invitee === yii::app()->user->getId()){
			$invite->completed = 1;
			if($invite->save()){
				$this->redirect(Yii::app()->request->urlReferrer);
			}
		}elseif (!isset($invite)) {
			throw new CHttpException(400,'The specified invitation code cannot be found.');
		} elseif (!yii::app()->user->getId() === $invite->invitee) {
			throw new CHttpException(400,'The specified invitation code was not sent to you');
		}
	}

	public function actionInviteCode(){
		$model = new InviteCode;
		if(isset($_POST['InviteCode'])){
			$model->attributes = $_POST['InviteCode'];
			if($model->validate()){
			    //NO ERRORS, SO WE PERFORM SAVE PROCESS
			    $this->redirect('accept', array('rString'=>$model->code));
			}else{
			    //TO SEE WHAT ERROR YOU HAVE
			    CVarDumper::dump($model->getErrors(),56789,true);
			    Yii::app()->end();
			    //an alternative way is to show attribute errors in view
			}
		}
		$this->render('inviteCode', array('model'=>$model,));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate($id = null)
	{
		if($id !== null){
			$model=new Invitations;

			// Uncomment the following line if AJAX validation is needed
			// $this->performAjaxValidation($model);

			if(isset($_POST['Invitations']))
			{
				$model->attributes=$_POST['Invitations'];
				$model->sender = yii::app()->user->getId();
				$user = User::model()->findByAttributes(array('email'=>$model->inviteeEmail));
				if(isset($user)){
					$model->invitee = $user->userid;
				}
				$model->team = $id;
				$model->rString = $this->generateString();
				if($model->save()){
					$teamName = Team::model()->findByPk($id)->teamName;
					$this->sendInvite($model->inviteeName, $model->inviteeEmail, $model->rString, $teamName);
					$this->redirect(array('view','id'=>$model->invitationid));
				}
			}

			$this->render('create',array('model'=>$model,));
		} else {
			throw new CHttpException(400,"No Team ID specified, \r\n Please create an invitation from the team management page");
		}
	}

	public function generateString(){
        $chars = array_merge(range(0, 9),  range('A', 'Z'), range('A', 'Z'));
        shuffle($chars);
        $string = implode(array_slice($chars, 0, 20));
        return $string;
	}

	public function sendInvite($invitee, $email, $rString, $teamName){
		//For testing purposes send every email to k0028502@tees.ac.uk
		$email = "k0028502@tees.ac.uk";

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// Additional headers
		$headers .= 'To: '.$invitee.' <'.$email.'>' . "\r\n";
		$headers .= 'From: no-reply@bugg.ly <no-reply@bugg.ly>' . "\r\n";

		$user = User::model()->findByPk(yii::app()->user->getId());
		$sender = $user->firstName.' '.$user->lastName;
		$message = "<html>
					<head>
						<title>Invitation Email</title>
					</head>
					<body>
					Hello $invitee,
					<br/>
					You have been invited by $sender to join us at Bugg.ly and take part in the Project Management as part of $teamName
					<br/>
					<br/>
					Click this link and login if you have already registered with us or follow the link and choose register.
					<br/>
					".CHtml::link('Bugg.ly Invitation', $this->createAbsoluteUrl('/invitations/accept', array('id'=>$rString)))."
					<br/>
					<br/>
					If that Doesnt Work copy and paste the below link into your browser and enter the invitation code manually.
					<br/>
					Web Link: ".$this->createAbsoluteUrl('/invitations/inviteCode/')."
					<br/>
					Invitation Code: $rString
					</body>
					</html>";
		mail($email, "Invitation to Join Team: ".$teamName." @ Bugg.ly", $message, $headers);
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Invitations']))
		{
			$model->attributes=$_POST['Invitations'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->invitationid));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Invitations');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Invitations('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Invitations']))
			$model->attributes=$_GET['Invitations'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Invitations the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Invitations::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Invitations $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='invitations-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
