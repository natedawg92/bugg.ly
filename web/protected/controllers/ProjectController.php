<?php

class ProjectController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'roles'=>array('guest'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view', 'create','update'),
				'roles'=>array('authenticated'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'roles'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$model = $this->loadModel($id);
		$user = User::model()->findByPk($model->projectOwner);
		$team = Team::model()->findByPk($model->projectTeam);
		$activeBugs = Bug::model()->findAllByAttributes(array('projectid'=>$model->projectid, 'active'=>true));
		$completedBUgs = Bug::model()->findAllByAttributes(array('projectid'=>$model->projectid, 'active'=>false));
		$activeBugLinks = array();
		$completeBugLinks = array();
		if(!empty($activeBugs)){
			foreach ($activeBugs as $b){
				array_push($activeBugLinks, CHtml::link($b->bugTitle, array('bug/view/', 'id'=>$b->bugid)));
			}
		} else {
			array_push($activeBugLinks, 'No active bugs');
		}
		if(!empty($completedBUgs)){
			foreach ($completedBUgs as $cb){
				array_push($completeBugLinks, CHtml::link($cb->bugTitle, array('bug/view/', 'id'=>$cb->bugid)));
			}
		} else {
			array_push($completeBugLinks, 'No completed bugs');
		}
		$this->render('view',array(
			'model'=>$model,
			'activeBugLinks'=>$activeBugLinks,
			'completeBugLinks'=>$completeBugLinks,
			'user'=>$user,
			'team'=>$team,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Project;
		$teamData = array();
		$member = TeamMembers::model()->findAllByAttributes(array('userid'=>yii::app()->user->getId()));
		foreach($member as $m){
			array_Push($teamData, Team::model()->findByPk($m->teamid));
		}
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			$model->projectOwner=yii::app()->user->getId();
			if($model->save())
				$this->redirect(array('view','id'=>$model->projectid));
		}

		$this->render('create',array('model'=>$model, 'teamData'=>$teamData));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$teamData = array();
		$member = TeamMembers::model()->findAllByAttributes(array('userid'=>yii::app()->user->getId()));
		foreach($member as $m){
			array_Push($teamData, Team::model()->findByPk($m->teamid));
		}

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Project']))
		{
			$model->attributes=$_POST['Project'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->projectid));
		}

		$this->render('update',array('model'=>$model, 'teamData'=>$teamData));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('Project');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Project('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Project']))
			$model->attributes=$_GET['Project'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Project the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Project::model()->with('owner')->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Project $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='project-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
