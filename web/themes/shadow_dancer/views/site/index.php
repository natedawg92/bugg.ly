<?php  
  $baseUrl = Yii::app()->theme->baseUrl; 
  $cs = Yii::app()->getClientScript();
  $cs->registerScriptFile('http://www.google.com/jsapi');
  $cs->registerCoreScript('jquery');
  $cs->registerScriptFile($baseUrl.'/js/jquery.gvChart-1.0.1.min.js');
  $cs->registerScriptFile($baseUrl.'/js/pbs.init.js');
  $cs->registerCssFile($baseUrl.'/css/jquery.css');

?>

<?php $this->pageTitle=Yii::app()->name; ?>

<h1><?php echo CHtml::encode(Yii::app()->name); ?>, The Bug Tracking Application</h1>
<div class="span-23 showgrid">
	<?php echo CHtml::image(Yii::app()->theme->baseUrl.'/images/track.png', 'Bug Tracking', array('width'=>'200px', 'class'=>'left')); ?>
	<div>
		<ul>
			<li>Quickly and easily Track Bugs through your Projects</li>
			<li>Bug Change history to track progression</li>
			<li>Collabarate with Teams</li>
			<li>Team Management</li>
		</ul>
	</div>
</div>